﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cue_Controller : MonoBehaviour
{
    //Memberfields
    [SerializeField]
    private GameObject target;              //Stores a Reference to the game object "Ball"

    private int lookSmooth = 200;           //Increases or Decreases the speed at which the Cue rotates

    private float verticalRotation = 0;     //Variable used to track vertical angle in degrees
    private float horizontalRotation = 0;   //Variable used to track horizontal angle in degrees

    private float vRotationMax = 45;        //Constraint on Vertical Rotation, cannot rotate to an angle higher than 45 degrees
    private float vRotationMin = -60;       //Constraint on Vertical Rotation, cannot rotate to an angle lower than -60

    private float offset = 2;               //The desired distance from the Ball
    private float verticalInput;            //Variable to hold input from the vertical axis
    private float horizontalInput;          //Variable to hold input from the horizontal axis

    public float VerticalRotation
    {
        get
        {
            return verticalRotation;
        }

        set
        {
            verticalRotation = value;
        }
    }

    public float HorizontalRotation
    {
        get
        {
            return horizontalRotation;
        }

        set
        {
            horizontalRotation = value;
        }
    }




    //We use Start() to Initialize the position of the Cue
    //to the location directly behind the Ball_PingPong
    void Start()
    {
        //The variable "destination" is set equal to the Ball_PingPong's "forward" vector (the direction that the Ball is "facing")
        // minus an offset value such that the Cue is behind the Ball_PingPong
        var destination = Quaternion.Euler(VerticalRotation, HorizontalRotation, 0) * -target.transform.forward * offset;

        //The "destination" vector is set equal to the Ball_PingPong's postion plus its' own vector
        destination += target.transform.position;

        //Then, the Cue position is updated to be equal to the "destination" vector
        transform.position = destination;

    }//END: Start()




    //We use the FixedUpdate function to receive Inputs from the Mouse
    //The String values of "MouseY etc..." are set in Unity via the Main Menu: Edit > Project Settings > Input > Inspector Pane
    //These String Values must be added to the List of Inputs and configured to receive input from the correct "Joystick" and "Axis"
    //Specifically, "Axis 4" = the Vertical Axis on the Right Analog Stick, and "Axis 5" = the Horizontal Axis on the Right Analog Stick
    //Also, we must specify "Joystick 1" as the joystick which originates all inputs
    //"MouseX" and "MouseY" are used for the vector coordinates obtained from the Mouse Motion
    private void FixedUpdate()
    {
        //receive inputs from the mouse movement
        verticalInput = Input.GetAxisRaw("Mouse Y");

        //same as above ...
        horizontalInput = Input.GetAxisRaw("Mouse X");

        //Increment the variables of vertical and horizontal rotation by
        //the amount that the player is moving the mouse
        //in those respective directions
        VerticalRotation += verticalInput * lookSmooth * Time.deltaTime;
        HorizontalRotation += horizontalInput * lookSmooth * Time.deltaTime;

        //Constrain the Vertical Rotation of the Cue
        //such that the Cue cannot pass under the Ball
        //or over the Ball
        if (VerticalRotation > vRotationMax)
        {
            VerticalRotation = vRotationMax;
        }
        if (VerticalRotation < vRotationMin)
        {
            VerticalRotation = vRotationMin;
        }


    }//End Update()



    //We use LateUpdate() to move the Cue (orbit the Ball)
    //and rotate it towards the target
    private void LateUpdate()
    {
        //As seen in Start(), we update the rotation of the camera's position by calculating the final position'
        //using the variable "destination"
        var destination = Quaternion.Euler(VerticalRotation, HorizontalRotation, 0) * -Vector3.forward * offset;
        destination += target.transform.position;
        transform.position = destination;

        //We point the camera towards the "target" gameobject
        transform.LookAt(target.transform);

    }//End LateUpdate()



}//END: Cue_Controller