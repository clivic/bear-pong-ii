﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ball_Controller : MonoBehaviour
{
    //Member Fields
    private Rigidbody m_rigidbody;

    [SerializeField]
    private bool m_ballOut;

    [SerializeField]
    private bool m_ballInCup;

    [SerializeField]
    private bool m_ballStopped;

    [SerializeField]
    private GameObject m_Cue;

    [SerializeField]
    private float m_speed;

    // External Fields
    [SerializeField]
    Game_Manager gameManager;

    //Use to Initialize Fields prior to the first frame of the game
    private void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody>();
        m_rigidbody.Sleep();
        m_speed = 50.0f;

    }//END: Awake()




    // Use this for initialization
    void Start()
    {
        m_ballInCup = false;
        m_ballOut = false;
        m_ballStopped = false;

        // Clarence
        gameManager = FindObjectOfType<Game_Manager>();

    }//END: Start()



    // Update is called once per frame
    void Update()
    {
        if (m_rigidbody.IsSleeping() && !m_Cue.activeInHierarchy)
        {
            m_ballStopped = true;
        }

        /*
        if (m_ballStopped)
        {
            SceneManager.LoadScene("Scene01");
        }
        */

    }//END: Update()



    private void FixedUpdate()
    {
        if (Input.GetMouseButton(0))
        {
            m_speed += 2.0f;
        }

        if (m_Cue.activeInHierarchy)
        {
            if (Input.GetMouseButtonUp(0))
            {
                Vector3 movement = m_Cue.transform.forward;
                m_rigidbody.AddForce(movement * m_speed);
                m_Cue.SetActive(false);

                // Clarence
                Invoke("LunchBall", 2f);
            }
        }

    }//END: FixedUpdate()

    public void LunchBall()
    {
        gameManager.BallLunched = true;
    }


    /// <summary>
    /// Tony: Pretty much what it says ... This method Resets the m_speed variable to its original starting value of 50.0f
    /// </summary>
    public void ResetSpeed()
    {
        m_speed = 50;   //To Fix Bug where the Speed continuously Increases with Every Turn, since Players keep adding to it on their turn.

    }





    //Detects collisions with the Bottom of the Cup
    private void OnCollisionEnter(Collision collision)
    {




    }//End OnCollisionEnter()

    private void OnTriggerEnter(Collider collider)
    {

        //"if" the Ball went into the Cup ...
        if (collider.CompareTag("Cup_Player2") || collider.CompareTag("Cup_Player1"))
        {
            //Print that the collision was detected to the console for debugging purposes
            GameObject winCanvas = GameObject.FindGameObjectWithTag("Win_Message");
            winCanvas.transform.GetChild(0).gameObject.SetActive(true);

            print("You WIN !!");
            //GetComponent<SphereCollider>().GetComponent<Material>(). = 0.0f;
            m_ballInCup = true;
            //gameManager.LoadMainMenu();
            Invoke("GoBackHome", 3f);
            //GoBackHome();
        }

        //"if" the Ball hit the Floor ...
        if (collider.CompareTag("Floor"))
        {
            //Print that the collision was detected to the console for debugging purposes
            print("Ball OUT !!");
            m_ballOut = true;

            gameManager.BallLunched = false;
            gameManager.ChangeActivePlayer();
            //print(gameManager.BallStopped());
        }
    }

    private void GoBackHome()
    {
        SceneManager.LoadScene("Scene00");
    }
}//END: Ball_Controller CLass