﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Controller : MonoBehaviour
{
    //Member Variables
    [SerializeField]
    private GameObject m_ball;

    private Vector3 m_offset;


    public GameObject Ball
    {
        get
        {
            return m_ball;
        }

        set
        {
            m_ball = value;
        }
    }

    //private Quaternion m_playerRotation;


    // Use this for initialization
    void Start()
    {
        SetCameraOffset();

        // Clarence Starts:
        m_ball = GameObject.FindGameObjectWithTag("Ball");

    }



    // Update is called once per frame
    void Update()
    {



    }



    void LateUpdate()
    {
        transform.position = Ball.transform.position + m_offset;

    }//END: LateUpdate()




    public void SetCameraOffset()
    {
        m_offset = transform.position - Ball.transform.position;

    }


}//END: Camera_Controller