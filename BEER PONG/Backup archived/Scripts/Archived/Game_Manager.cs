﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


/*  The Game_Manager object is responsible for detecting the following, every frame,
 * within the: Update(), FixedUpdate(), and LateUpdate() methods.
 * Detects: (Methods defined)
 *          The number of Cups remaining in play for Player 1 and Player 2
 *          Whether the Ball is "On the Floor"
 *          Whether the Ball is "In a Cup"
 *          Whether the Ball has "Stopped Moving"
 *          What Cup the Ball is in
 * 
 * Game_Manager loads the Main Menu (otherwise known as: "Scene00") if a winner is declared (by detecing that one player does not have any cups remaining).
 *      * Game_Manager displays HUD message: "Player <1 or 2> YOU WIN !!" or something, in the event that the player has "plunked" all of the opponent's cups.
 * 
 * Game_Manager, after detecing the Game State, changes the active player's turn to
 *  the opposing player, if play continues.
 * Game_Manager then resets the Ball position to the starting position of that respective player.
 * 
 */
public class Game_Manager : MonoBehaviour
{
    //Member Fields
    [SerializeField] //To Display to Unity Inspector - for testing purposes
    int m_player1Cups;  //holds the number of Cups that Player 1 currently has in play

    [SerializeField] //To Display to Unity Inspector - for testing purposes
    int m_player2Cups;  //holds the number of Cups that Player 2 currently has in play

    [SerializeField] //To Display to Unity Inspector - for testing purposes
    GameObject m_plunkedCup;    //holds the cup that the Ball landed in

    [SerializeField] //To Display to Unity Inspector - for testing purposes
    bool m_ballInCup;   // tracks if the Ball is in a Cup
    [SerializeField] //To Display to Unity Inspector - for testing purposes
    bool m_ballOnFloor; // tracks if the Ball is on the Floor
    [SerializeField] //To Display to Unity Inspector - for testing purposes
    bool m_ballStopped; // tracks if the Ball has Stopped Moving
    [SerializeField] //To Display to Unity Inspector - for testing purposes
    bool m_ballLunched; // tracks if the Ball has been fired

    GameObject m_activePlayer; // tracks whose turn it is currently

    [SerializeField]        // Set this Field = the Player 1 GameObject, within Unity Inspector
    GameObject m_player1;       // holds a reference to the "Player1" GameObject

    [SerializeField]        //Set this Field = the Player 2 GameObject, within Unity Inspector
    GameObject m_player2;       // holds a reference to the "Player2" GameObject

    [SerializeField]        // Set this Field = the GameObject "Player1BallStartingPosition" within Unity Inspector
    GameObject m_player1BallStartingPosition;   //Holds a Reference to An Empty GameObject in the Scene, whose Transform's Position is the Starting Position of Player 1's Ball

    [SerializeField]        // Set this Field = the GameObject "Player2BallStartingPostion" within Unity Inspector
    GameObject m_player2BallStartingPosition;   //Holds a Reference to An Empty GameObject in the Scene, whose Transform's Position is the Starting Position of Player 2's Ball

    [SerializeField]        // Set this Field = the GameObject "Ball" within Unity Inspector
    GameObject m_Ball;      //Holds a Reference to the Ball GameObject

    [SerializeField]
    GameObject m_Cue;   //Holds a Reference to the "Cue" object in the Scene

    GameObject m_camera;    //Holds a Reference to the "Main Camera" in the Scene

    /// <summary>
    /// Clarence: This is used to determine when the ball is deemed as stopped.
    /// </summary>
    [SerializeField]
    float m_thresholdBallStop = 0.5f;

    Vector3 m_cameraStartPosition = new Vector3(0, 36, -40);
    Vector3 m_cameraStartRotation = new Vector3(36, 0, 0);

    public bool BallLunched
    {
        get
        {
            return m_ballLunched;
        }

        set
        {
            m_ballLunched = value;
        }
    }


    // Use this for initialization
    void Start()
    {
        // Find objects
        m_Ball = GameObject.FindGameObjectWithTag("Ball");
        m_player1BallStartingPosition = GameObject.FindGameObjectWithTag("P1_Start");
        m_player2BallStartingPosition = GameObject.FindGameObjectWithTag("P2_Start");
        m_player1 = GameObject.FindGameObjectWithTag("Player1");
        m_player2 = GameObject.FindGameObjectWithTag("Player2");
        m_Cue = GameObject.FindGameObjectWithTag("Cue");
        m_camera = GameObject.FindGameObjectWithTag("MainCamera");
        m_activePlayer = m_player1;
        //ChangeActivePlayer();

    }//END: Start()


    // Update is called once per frame
    void Update()
    {
        DetectGaemState();

    }//END: Update()


    // FixedUpdate is used for all Physics Related processing
    private void FixedUpdate()
    {
        if (BallStopped())
        {
            ChangeActivePlayer();
            //print("Reset ball");
        }

    }//END: FixedUpdate()


    // LateUpdate is used for detecting where objects are in the scene, after all objects have calculated their final positions
    private void LateUpdate()
    {



    }//END: LateUpdate()


    //******************************************************************
    //  Method Signatures
    //******************************************************************

    // Detect_Game_State Calls:         TBD
    //      NumberOfCupsForPlayer1
    //      NumberOfCupsForPlayer2
    void DetectGaemState()
    {
        m_player1Cups = NumberOfCupsForPlayer1();
        m_player2Cups = NumberOfCupsForPlayer2();

        //// Clarence Starts:
        //if (m_ballOnFloor)
        //{
        //    ChangeActivePlayer();
        //}

        // Clarence Ends.

    }//END: DetectGameState()

    //*****************************************************************************************************************

    // NumberOfCupsForPlayer1 returns the number of cups remaining in play for Player1 - JOSIAH -  fixed cups - use TAGS
    public int NumberOfCupsForPlayer1()
    {
        int cups = 0;


        return cups;

    }//END: NumberOfCupsForPlayer1

    //*****************************************************************************************************************

    // NumberOfCupsForPlayer2 returns the number of cups remaining in play for Player2 -JOSIAH
    public int NumberOfCupsForPlayer2()
    {
        int cups = 0;


        return cups;

    }//END: NumberOfCupsForPlayer2

    //*****************************************************************************************************************

    // BallInCup detects if the Ball has landed inside of a Cup (refered to as a "Plunk") -figure stuff out with ABOVE METHODS JOSIAH
    public bool BallInCup()
    {


        return m_ballInCup;

    }//END: BallInCup()

    //*****************************************************************************************************************

    // PlunkedCup returns the cup that the ball just landed in JOSIAH
    public GameObject PlunkedCup()
    {

        return m_plunkedCup;

    }//END: PlunkedCup()

    //*****************************************************************************************************************

    /// <summary>
    /// CLARENCE: BallOnFloor returns a boolean that tracks if the Ball is on the Floor 
    /// </summary>
    /// <remarks>CLARENCE: I think we should discard this function. We already have the ball detect whether hit floor.</remarks>
    /// <param name="collider"></param>
    /// <returns>m_ballOnFloors</returns>
    public bool BallOnFloor(Collider collider)
    {
        if (collider.tag == "Floor")
        {
            m_ballOnFloor = true;
        }

        return m_ballOnFloor;

    }//END: BallOnFloor()

    //*****************************************************************************************************************

    /// <summary>
    /// CLARENCE [TESTED]: BallStopped returns a boolean that tracks if the Ball has Stopped Moving 
    /// </summary>
    /// <returns>m_ballStopped</returns>
    public bool BallStopped()
    {
        if (!BallLunched) return false;
        Rigidbody rigidbody = m_Ball.GetComponent<Rigidbody>();
        if (rigidbody.velocity.magnitude < m_thresholdBallStop && !m_ballInCup) m_ballStopped = true;
        //print(rigidbody.velocity.magnitude);
        //print(m_ballStopped);
        return m_ballStopped;

    }//END: BallStopped()

    //*****************************************************************************************************************

    /// <summary>
    /// CLARENCE [TESTED]: ChangeActivePlayer sets m_activePlayer to the opposing player
    /// </summary>
    public void ChangeActivePlayer()
    {
        if (m_activePlayer == m_player1)
        {
            m_activePlayer = m_player2;
        }
        else m_activePlayer = m_player1;

        // Initialization of ball and camera
        m_ballOnFloor = false;
        m_ballInCup = false;
        m_ballStopped = false;
        m_ballLunched = false;
        //SceneManager.LoadScene("Scene01");
        ResetBallPosition();
        SetCamera();

    }//END: ChangeActivePlayer()

    //*****************************************************************************************************************

    // ResetBallPosition changes the position of the Ball to that of the Active Player's Ball's starting position
    /// <summary>
    /// Tony: Fixed Resetting the Ball's Location at the Begining of Each Turn, and keeping it "asleep"
    /// </summary>
    public void ResetBallPosition()
    {
        m_Ball.SetActive(false);

        if (m_activePlayer == m_player1)
        {
            m_Ball.transform.SetPositionAndRotation(m_player1BallStartingPosition.transform.position, m_player1BallStartingPosition.transform.rotation);
            //print("player 1's turn");
        }

        if (m_activePlayer == m_player2)
        {
            m_Ball.transform.SetPositionAndRotation(m_player2BallStartingPosition.transform.position, m_player2BallStartingPosition.transform.rotation);
            //print("player 2's turn");
        }

        m_Ball.GetComponent<Ball_Controller>().ResetSpeed();
        m_Ball.SetActive(true);
        m_Ball.GetComponent<Rigidbody>().Sleep();
        m_Cue.SetActive(true);
        m_Cue.GetComponent<Cue_Controller>().HorizontalRotation += 180;
        m_Cue.GetComponent<Cue_Controller>().VerticalRotation += 180;

    }//END: ResetBallPosition()

    //*****************************************************************************************************************

    //LoadMainMenu loads Scene00 ... which happens to also be the Main Menu.
    public void LoadMainMenu()
    {
        SceneManager.LoadScene("Scene00");

    }//END: LoadMainMenu()

    //*****************************************************************************************************************

    // Private Tool functions
    /// <summary>
    /// Clarence: Set up the camera after each round
    /// Tony: Fixed Problem with Camera Facing the Wrong Direction --> Now it Faces the "Right" Way
    /// </summary>
    private void SetCamera()
    {
        //print("SetCamera");
        //print(m_activePlayer.tag);
        if (m_activePlayer.CompareTag("Player1"))
        {
            Camera.main.transform.position = m_cameraStartPosition;
            Camera.main.transform.eulerAngles = m_cameraStartRotation;

            m_camera.GetComponent<Camera_Controller>().SetCameraOffset();

        }
        else
        {
            Vector3 cameraStartPosition = m_cameraStartPosition;
            Vector3 cameraStartRotation = m_cameraStartRotation;
            cameraStartPosition.z = -cameraStartPosition.z;
            cameraStartRotation.y = 180;

            Camera.main.transform.position = cameraStartPosition;
            Camera.main.transform.eulerAngles = cameraStartRotation;

            m_camera.GetComponent<Camera_Controller>().SetCameraOffset();
        }
    }

}//END: Game_Manager Class
