﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour
{
    [SerializeField]
    Vector3 ballStartPosition;

    [SerializeField]
    Vector3 cameraStartPosition;

    [SerializeField]
    Vector3 cueStartPosition;

    //[SerializeField]
    //GameObject cupPrefab;

    //[SerializeField]
    //GameObject ballPrefab;

    [SerializeField]
    string characterName;

    [SerializeField]
    BarController drunkennessBar;

    [SerializeField]
    CupDisplayer cupDisplayer;

    [SerializeField]
    float maxDrunkenness = 100;

    [SerializeField]
    float currentDrunkenness;

    [SerializeField]
    Sprite avatar;

    [SerializeField]
    Image avatarRenderer;

    [SerializeField]
    Text characterNameDisplayer;

    public Vector3 BallStartPosition
    {
        get
        {
            return ballStartPosition;
        }
    }

    //public GameObject CupPrefab
    //{
    //    get
    //    {
    //        return cupPrefab;
    //    }
    //}

    //public GameObject BallPrefab
    //{
    //    get
    //    {
    //        return ballPrefab;
    //    }
    //}

    public Vector3 CameraStartPosition
    {
        get
        {
            return cameraStartPosition;
        }
    }

    public Vector3 CueStartPosition
    {
        get
        {
            return cueStartPosition;
        }
    }

    public List<Cup> Cups
    {
        get; private set;
    }

    public string CharacterName
    {
        get
        {
            return characterName;
        }

        set
        {
            characterName = value;
            if (characterNameDisplayer)
                characterNameDisplayer.text = characterName;
        }
    }

    public float CurrentDrunkenness
    {
        get
        {
            return currentDrunkenness;
        }

        set
        {
            currentDrunkenness = Mathf.Clamp(value, 0, MaxDrunkenness);

            drunkennessBar.Value = currentDrunkenness;
        }
    }

    public float MaxDrunkenness
    {
        get
        {
            return maxDrunkenness;
        }

        set
        {
            maxDrunkenness = value;
            drunkennessBar.MaxValue = maxDrunkenness;
        }
    }

    public void UpdateCupDisplay(float time = .0f)
    {
        cupDisplayer.CheckCups();
    }

    private void Start()
    {
        //cups = transform.GetComponentsInChildren<Cup>();
        ballStartPosition = gameObject.FindChildWithTag("BallStartingPosition").transform.position;
        cueStartPosition = gameObject.FindChildWithTag("CueStartingPosition").transform.position;
        cameraStartPosition = gameObject.FindChildWithTag("CameraStartingPosition").transform.position;

        MaxDrunkenness = maxDrunkenness;
        CurrentDrunkenness = currentDrunkenness;

        Cups = new List<Cup>();
        Cups.AddRange(transform.GetComponentsInChildren<Cup>());

        if (avatarRenderer)
        {
            avatarRenderer.sprite = avatar;
        }

        CharacterName = characterName;
    }
}
