﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Main_Menu_Controller : MonoBehaviour
{
    private void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    //StartGame() loads a Scene of the game
    public void StartGame()
    {
        SceneManager.LoadScene("Scene01");
    }//End StartGame()


    //QuitGame() closes the application
    public void QuitGame()
    {
        Application.Quit();
    }//End QuitGame()


}//END: Main_Menu_Controller Class