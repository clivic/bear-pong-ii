﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    Rigidbody rb;

    [SerializeField]
    [Range(0, 100f)]
    float initialSpeed = 50.0f; // Every round, ball's starting speed.

    [SerializeField]
    float currentSpeed;

    [SerializeField]
    [Range(100, 250f)]
    float maxSpeed;

    [SerializeField]
    float stopSpeedThreshold = 2f;

    int framesToWairAfterLaunchToCheckSpeed = 20;

    bool ballLaunched;

    AudioSource audioSource;

    [SerializeField]
    BarController powerBar;

    public bool BallLaunched
    {
        set
        {
            ballLaunched = value;
            if (ballLaunched)
                StartCoroutine(CheckBallSpeed(framesToWairAfterLaunchToCheckSpeed));
        }
    }

    public float Thrust
    {
        get
        {
            return currentSpeed;
        }
        set
        {
            currentSpeed = Mathf.Clamp(value, 0, MaxPower);
            powerBar.Value = currentSpeed;
        }
    }

    public float MaxPower
    {
        get
        {
            return maxSpeed;
        }
        set
        {
            maxSpeed = value;
            powerBar.MaxValue = maxSpeed;
        }
    }

    public float Speed
    {
        get
        {
            return GetComponent<Rigidbody>().velocity.magnitude;
        }
    }

    IEnumerator CheckBallSpeed(float waitingFrames)
    {
        for (int i = 0; i < waitingFrames; i++)
        {
            yield return new WaitForFixedUpdate();
        }
        while (Speed > stopSpeedThreshold)
        {
            yield return new WaitForFixedUpdate();
        }
        GameManager.Instance.NextRound();
    }

    public Rigidbody Rb
    {
        get
        {
            return rb;
        }

        set
        {
            rb = value;
        }
    }

    private void Awake()
    {
        //InactivateBall();
        audioSource = GetComponent<AudioSource>();
    }

    public void ResetStatus()
    {
        rb = GetComponent<Rigidbody>();
        rb.isKinematic = true;
        rb.velocity = Vector3.zero;

        MaxPower = maxSpeed;
        Thrust = initialSpeed;
    }

    private void Start()
    {
        ResetStatus();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Cup"))
        {
            // Drink up the cup of wine
            Character player = other.transform.parent.GetComponent<Character>();
            float howMuchDrunkennessGot = other.GetComponent<Cup>().CupDrunkenness;
            player.CurrentDrunkenness += howMuchDrunkennessGot;

            // remove the cup
            player.Cups.Remove(other.GetComponent<Cup>());
            Destroy(other.gameObject);

            GameManager.Instance.NextRound();

            // Update UI
            player.UpdateCupDisplay(2);
        }

        if (other.CompareTag("Floor"))
        {
            // go to next round
            GameManager.Instance.NextRound();
        }
    }

    /// <summary>
    /// Plays the SFX of the Ball colliding with a Surface
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        audioSource.Play();

    }






}
