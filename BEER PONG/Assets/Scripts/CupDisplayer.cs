﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CupDisplayer : MonoBehaviour
{
    [SerializeField]
    GameObject cupUIPrefab;

    [SerializeField]
    Character player;

    private void Start()
    {
    }

    public void CheckCups(float time = .0f)
    {
        if (time > 0)
        {
            Invoke("CheckCups", time);
        }

        int howMany = player.Cups.Count;

        foreach (Transform t in transform)
        {
            Destroy(t.gameObject);
        }

        for (int i = 0; i < howMany; i++)
        {
            GameObject cup = Instantiate(cupUIPrefab);
            cup.transform.parent = transform;
        }
    }
}
