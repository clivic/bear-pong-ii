﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CueController : MonoBehaviour
{
    GameObject ball;

    private float verticalOffset = 0;
    private float horizontalOffset = 0;

    //private float vRotationMax = 45;                //Constraint on Vertical Rotation, cannot rotate to an angle higher than 45 degrees
    //private float vRotationMin = -60;               //Constraint on Vertical Rotation, cannot rotate to an angle lower than -60

    [SerializeField]
    private float distanceFromTheBall = 2;               //The desired distance from the Ball

    [SerializeField]
    private float mouseSensitivity = 2;

    private Vector3 startPosition;

    [SerializeField]
    private Vector2 drunknessDirection;
    [SerializeField]
    private float drunknessIncrementX;
    [SerializeField]
    private float drunknessIncrementY;

    float currentDrunkness;

    public float DistanceFromTheBall
    {
        get
        {
            return distanceFromTheBall;
        }

        set
        {
            distanceFromTheBall = value;
        }
    }

    public float VerticalOffset
    {
        get
        {
            return verticalOffset;
        }

        set
        {
            verticalOffset = value;
        }
    }

    public float HorizontalOffset
    {
        get
        {
            return horizontalOffset;
        }

        set
        {
            horizontalOffset = value;
        }
    }

    public float MouseSensitivity
    {
        get
        {
            return mouseSensitivity;
        }

        set
        {
            mouseSensitivity = value;
        }
    }

    public bool Focus
    {
        set
        {
            if (value)
            {
                StartCoroutine(FocusWhenDrunk());
            }
            else
            {
                StopCoroutine(FocusWhenDrunk());
                SetDrunkness(currentDrunkness);
            }
        }
    }

    private void Start()
    {
        ball = GameObject.FindGameObjectWithTag("Ball");
        RestCue();
    }

    public void UpdateCue()
    {
        transform.position = startPosition + new Vector3(HorizontalOffset + drunknessDirection.x, VerticalOffset + drunknessDirection.y);

        transform.LookAt(ball.transform);
    }

    public void RestCue(float drunkenness = 0)
    {
        //print("The ball of the cue is " + ball);
        VerticalOffset = 0;
        HorizontalOffset = 0;
        GameManager.Instance.MouseFreeMove = false;
        //Cursor.lockState = CursorLockMode.None;

        Vector3 newPos = GameManager.Instance.ActivePlayer.CueStartPosition;

        startPosition = transform.position = newPos;

        SetDrunkness(drunkenness);
        drunknessDirection = new Vector2();
        StopCoroutine(ChangeDrunknessDirection());
        StartCoroutine(ChangeDrunknessDirection());
    }

    public void SetDrunkness(float drunkenness, bool temporary = false)
    {
        if (!temporary)
            currentDrunkness = drunkenness;
        drunknessIncrementX = Mathf.Pow(drunkenness, 1.5f) / 10000;
        drunknessIncrementY = Mathf.Pow(drunkenness, 1.5f) / 10000;
    }

    private void FixedUpdate()
    {
        // Adjust the drunkness direction
        drunknessDirection += new Vector2(drunknessIncrementX, drunknessIncrementY);
        drunknessDirection.x = Mathf.Clamp(drunknessDirection.x, -20, 20);
        drunknessDirection.y = Mathf.Clamp(drunknessDirection.y, -20, 20);
    }

    IEnumerator ChangeDrunknessDirection()
    {
        for (; ; )
        {
            float chanceX = Random.Range(-1f, 1f);
            float chanceY = Random.Range(-1f, 1f);
            if (chanceX > 0) drunknessIncrementX = Mathf.Abs(drunknessIncrementX);
            else drunknessIncrementX = -Mathf.Abs(drunknessIncrementX);
            if (chanceY > 0) drunknessIncrementY = Mathf.Abs(drunknessIncrementY);
            else drunknessIncrementY = -Mathf.Abs(drunknessIncrementY);

            print($"{chanceX}, {chanceY}");

            float ws = Random.Range(0f, currentDrunkness != 0 ? 50f / currentDrunkness : 5f);
            yield return new WaitForSeconds(ws);
        }
    }

    IEnumerator FocusWhenDrunk()
    {
        float focusedDrunkenness = currentDrunkness * .2f;
        //float originalDrunkenness = currentDrunkness;
        SetDrunkness(focusedDrunkenness, true);

        yield return new WaitForSeconds(Random.Range(1f, 5f));

        SetDrunkness(currentDrunkness);

    }
}
