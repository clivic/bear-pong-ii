﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : Singleton<GameManager>
{
    public enum GameState { Prepration, OnGoing }

    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float trajectoryOffset = .22f;

    [SerializeField]
    GameState gameState;

    List<Character> players;

    [SerializeField]
    int indexOfActivePlayer;

    BallController ball;
    CueController cue;

    GameObject uiCanvas;

    bool mouseFreeMove;

    public Character ActivePlayer
    {
        get
        {
            return players[indexOfActivePlayer];
        }
    }

    public GameState BallGameState
    {
        get
        {
            return gameState;
        }
    }

    public BallController Ball
    {
        get
        {
            return ball;
        }
    }

    public CueController Cue
    {
        get
        {
            return cue;
        }
    }

    public void NextRound()
    {
        if (!CheckWinningState())
        {
            if (gameState == GameState.OnGoing)
            {
                gameState = GameState.Prepration;
                Invoke("InitiateNextRound", 2f);
            }
        }
    }

    bool CheckWinningState()
    {
        for (int i = 0; i < players.Count; i++)
        {
            if (players[i].Cups.Count < 1)
            {
                // Out!
                StartCoroutine(DisplayLoseMessage(players[i]));

                // This player runs out of cups!
                players.RemoveAt(i);
            }
        }

        if (players.Count == 1)
        {
            // Winner!
            StartCoroutine(DisplayWinMessage(players[0]));
            return true;
        }

        if (players.Count < 1)
        {
            // No winner. (Currently imposible)
            StartCoroutine(DisplayDrawMessage());
            return true;
        }

        return false;
    }

    void InitiateNextRound()
    {
        // Set the active player the next one.
        indexOfActivePlayer = (indexOfActivePlayer + 1) % players.Count;
        ResetForPreparation();

        print("Game manager gets the ball:" + ball);

        print("next round!");
    }

    public void ResetTheRound()
    {
        if (gameState == GameState.Prepration) return;

        gameState = GameState.Prepration;
        ResetForPreparation();
    }

    private void ResetForPreparation()
    {
        SetBallForActivePlayer();
        SetCameraForActivePlayer();
        SetCueForActivePlayer();
    }

    private void UpdateTrajectory(Vector3 initialPosition, Vector3 initialVelocity, Vector3 gravity)
    {
        int numSteps = 20; // How large the trace is
        float timeDelta = 1.0f / initialVelocity.magnitude; // Amount of seconds the after trace is made

        LineRenderer lineRenderer = ball.GetComponent<LineRenderer>();
        lineRenderer.SetVertexCount(numSteps);

        Vector3 position = initialPosition;
        Vector3 velocity = initialVelocity;
        for (int i = 0; i < numSteps; ++i)
        {
            lineRenderer.SetPosition(i, position);

            position += velocity * timeDelta + 0.5f * gravity * timeDelta * timeDelta; //-= FOR TRAILING += FOR PREDICTION
            velocity += gravity * timeDelta;
        }
    }

    public bool MouseFreeMove
    {
        get { return mouseFreeMove; }
        set
        {
            if (value)
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
            mouseFreeMove = value;
        }
    }

    private void Start()
    {
        players = new List<Character>()
        {
            GameObject.FindGameObjectWithTag("Player1").GetComponent<Character>(),
            GameObject.FindGameObjectWithTag("Player2").GetComponent<Character>()
        };

        indexOfActivePlayer = 0;

        GameObject b = GameObject.FindGameObjectWithTag("Ball");
        //if (!b) GetBallFromActivePlayer();
        /*else*/
        ball = b.GetComponent<BallController>();

        cue = GameObject.FindGameObjectWithTag("Cue").GetComponent<CueController>();

        uiCanvas = FindObjectOfType<Canvas>().gameObject;

        gameState = GameState.Prepration;

        foreach (Character player in players)
        {
            player.UpdateCupDisplay();
        }
    }

    private void Update()
    {
        // If the mouse is over a UI element in the scene, do nothing. 
        // This means when the player clicks a UI button, it won't trigger this mouse click event.
        if (EventSystem.current.IsPointerOverGameObject()) return;

        if (Input.GetButton("Fire1"))
        {
            if (BallGameState == GameState.Prepration)
            {
                ball.Thrust += 3.0f;
            }
        }

        if (Input.GetButtonUp("Fire1"))
        {
            if (BallGameState == GameState.Prepration)
            {
                Vector3 movement = cue.transform.forward;
                ball.GetComponent<Rigidbody>().isKinematic = false;
                ball.GetComponent<Rigidbody>().AddForce(movement * ball.Thrust);
                cue.gameObject.SetActive(false);
                gameState = GameState.OnGoing;
                ball.BallLaunched = true;

                MouseFreeMove = true;
            }
        }

        if (Input.GetButtonDown("Fire3"))
        {
            if (BallGameState == GameState.Prepration)
            {
                cue.Focus = true;
            }
        }

        if (Input.GetButtonUp("Fire3"))
        {
            if (BallGameState == GameState.Prepration)
            {
                cue.Focus = false;
            }
        }
    }

    private void FixedUpdate()
    {
        float horizontalInput = Input.GetAxisRaw("Mouse X");
        float verticalInput = Input.GetAxisRaw("Mouse Y");

        cue.VerticalOffset += verticalInput * cue.MouseSensitivity * Time.deltaTime;
        if (indexOfActivePlayer == 0)
        {
            cue.HorizontalOffset += horizontalInput * cue.MouseSensitivity * Time.deltaTime;
        }
        else
        {
            cue.HorizontalOffset -= horizontalInput * cue.MouseSensitivity * Time.deltaTime;
        }

        UpdateTrajectory(ball.transform.position, cue.transform.forward * ball.Thrust * trajectoryOffset, Physics.gravity);
        if (!ball.Rb.isKinematic)
        {
            LineRenderer lineRenderer = ball.GetComponent<LineRenderer>(); //deletes the linerenderer
            lineRenderer.SetVertexCount(0);
        }

    }

    private void LateUpdate()
    {
        cue.UpdateCue();
    }

    private void SetBallForActivePlayer()
    {
        ball.ResetStatus();

        ball.transform.position = ActivePlayer.BallStartPosition;
    }

    private void SetCameraForActivePlayer()
    {
        Camera.main.transform.position = ActivePlayer.CameraStartPosition;
        Camera.main.transform.eulerAngles = new Vector3(Camera.main.transform.eulerAngles.x, (indexOfActivePlayer * 180) % 360, 0);

        CameraController cameraController = Camera.main.GetComponent<CameraController>();
        cameraController.SetCameraOffset();
        cameraController.ResetDrunkness(ActivePlayer.CurrentDrunkenness);
    }

    private void SetCueForActivePlayer()
    {
        cue.gameObject.SetActive(true);
        cue.RestCue(ActivePlayer.CurrentDrunkenness);
    }

    IEnumerator DisplayLoseMessage(Character character)
    {
        Text loseMessage = uiCanvas.transform.GetChild(1).gameObject.GetComponent<Text>();
        loseMessage.text = $"{character.CharacterName}{Environment.NewLine} is out!";
        loseMessage.gameObject.SetActive(true);

        yield return new WaitForSeconds(2);

        loseMessage.gameObject.SetActive(false);
    }

    IEnumerator DisplayWinMessage(Character character)
    {
        //Print that the collision was detected to the console for debugging purposes
        Text winMessage = uiCanvas.transform.GetChild(0).gameObject.GetComponent<Text>();
        winMessage.text = $"{players[0].CharacterName}{Environment.NewLine} wins!";
        winMessage.gameObject.SetActive(true);

        yield return new WaitForSeconds(3);

        GoBackHome();
    }

    IEnumerator DisplayDrawMessage()
    {
        Text drawMessage = uiCanvas.transform.GetChild(0).gameObject.GetComponent<Text>();
        drawMessage.text = "Draw!";
        drawMessage.gameObject.SetActive(true);

        yield return new WaitForSeconds(3);

        GoBackHome();
    }

    private void GoBackHome()
    {
        SceneManager.LoadScene("Scene00");
    }
}


///// <summary>
///// Deprecated.
///// If there is no ball in the scene, generate one from the active player;
///// </summary>
//private void GetBallFromActivePlayer()
//{
//    ball = Instantiate(ActivePlayer.BallPrefab, ActivePlayer.BallStartPosition, Quaternion.identity).GetComponent<BallController>();
//    ball.tag = "Ball";
//    ball.GetComponent<Rigidbody>().isKinematic = true;
//}