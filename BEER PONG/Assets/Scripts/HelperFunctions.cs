﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HelperFunctions
{

    public static GameObject FindChildWithTag(this GameObject parent, string tag)
    {
        Transform transform = parent.transform;
        foreach (Transform t in transform)
        {
            if (tag == t.tag)
                return t.gameObject;
        }
        return null;
    }

    public static T FindComponentInChildWithTag<T>(this GameObject parent, string tag) where T : Component
    {
        Transform transform = parent.transform;
        foreach (Transform t in transform)
        {
            if (tag == t.tag)
            {
                return t.GetComponent<T>();
            }
        }
        return null;
    }

}