﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarController : MonoBehaviour
{

    [SerializeField]
    private Image content;

    [SerializeField]
    private Text valueText;

    /// <summary>
    /// The movement speed of the bar
    /// </summary>
    [SerializeField]
    private float lerpSpeed;

    private float fillAmount;

    /// <summary>
    /// Inidcates the max value of the bar, this can reflect the player's max drunkenness etc.
    /// </summary>
    public float MaxValue { get; set; }

    public float Value
    {
        set
        {
            fillAmount = Map(value, 0, MaxValue, 0, 1);
        }
    }

    private void UpdateBar()
    {
        if (fillAmount != content.fillAmount)
        {
            content.fillAmount = Mathf.Lerp(content.fillAmount, fillAmount, Time.deltaTime * lerpSpeed);
        }
    }

    /// <summary>
    /// Maps a range of number into another range
    /// </summary>
    /// <param name="value">The value to evaluate</param>
    /// <param name="inMin">The minimum value of the evaluated variable</param>
    /// <param name="inMax">The maximum value of the evaluated variable</param>
    /// <param name="outMin">The minum number we want to map to</param>
    /// <param name="outMax">The maximum number we want to map to</param>
    /// <returns></returns>
    private float Map(float value, float inMin, float inMax, float outMin, float outMax)
    {
        return (value - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
    }

    private void Update()
    {
        UpdateBar();
    }
}
