﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    GameObject ball;

    Vector3 offset;

    AudioSource audioSource;

    AudioSource[] musicList;

    int musicListIndex = 0;

    [SerializeField]
    private Vector2 drunknessDirection;
    [SerializeField]
    private float drunknessIncrementX;
    [SerializeField]
    private float drunknessIncrementY;

    float currentDrunkness;

    public GameObject Ball
    {
        get
        {
            return ball;
        }

        set
        {
            ball = value;
        }
    }

    private void Awake()
    {
        musicList = GetComponents<AudioSource>();
        audioSource = musicList[0];
    }

    private void Start()
    {
        Ball = GameObject.FindGameObjectWithTag("Ball");
        SetCameraOffset();
        audioSource.Play();
    }

    void LateUpdate()
    {
        transform.position = Ball.transform.position + offset + (Vector3)drunknessDirection;

        if (!audioSource.isPlaying)
        {
            musicListIndex++;

            if (musicListIndex > musicList.Length - 1)
                musicListIndex = 0;

            //musicListIndex = (musicListIndex + 1) % musicList.Length;

            audioSource = musicList[musicListIndex];
            audioSource.Play();
        }

    }

    public void ResetDrunkness(float drunkenness = 0)
    {
        SetDrunkness(drunkenness);
        drunknessDirection = new Vector2();
        StopCoroutine(ChangeDrunknessDirection());
        StartCoroutine(ChangeDrunknessDirection());
    }

    public void SetCameraOffset()
    {
        offset = transform.position - Ball.transform.position;

    }

    public void SetDrunkness(float drunkenness, bool temporary = false)
    {
        if (!temporary)
            currentDrunkness = drunkenness;
        drunknessIncrementX = Mathf.Pow(drunkenness, 1.1f) / 10000;
        drunknessIncrementY = Mathf.Pow(drunkenness, 1.1f) / 10000;
    }

    private void FixedUpdate()
    {
        // Adjust the drunkness direction
        drunknessDirection += new Vector2(drunknessIncrementX, drunknessIncrementY);
        drunknessDirection.x = Mathf.Clamp(drunknessDirection.x, -10, 10);
        drunknessDirection.y = Mathf.Clamp(drunknessDirection.y, -10, 10);
    }

    IEnumerator ChangeDrunknessDirection()
    {
        for (; ; )
        {
            float chanceX = Random.Range(-1f, 1f);
            float chanceY = Random.Range(-1f, 1f);
            if (chanceX > 0) drunknessIncrementX = Mathf.Abs(drunknessIncrementX);
            else drunknessIncrementX = -Mathf.Abs(drunknessIncrementX);
            if (chanceY > 0) drunknessIncrementY = Mathf.Abs(drunknessIncrementY);
            else drunknessIncrementY = -Mathf.Abs(drunknessIncrementY);

            print($"{chanceX}, {chanceY}");

            float ws = Random.Range(0f, currentDrunkness != 0 ? 50f / currentDrunkness : 5f);
            yield return new WaitForSeconds(ws);
        }
    }

    IEnumerator FocusWhenDrunk()
    {
        float focusedDrunkenness = currentDrunkness * .2f;
        //float originalDrunkenness = currentDrunkness;
        SetDrunkness(focusedDrunkenness, true);

        yield return new WaitForSeconds(Random.Range(1f, 5f));

        SetDrunkness(currentDrunkness);

    }
}
